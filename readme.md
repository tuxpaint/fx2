# fx2

the framework https://github.com/uber-go/fx needs to maintain backwards compatibility.

as a result, it cannot add a new default dependency.

instead of reimplementing these same basically-default dependencies in every repo, it would be better to reuse them here.

so we do that.
