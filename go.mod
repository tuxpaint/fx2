module anime.bike/fx2

go 1.22.0

require go.uber.org/fx v1.22.1

require (
	go.uber.org/dig v1.17.1 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	go.uber.org/zap v1.27.0 // indirect
	golang.org/x/sys v0.21.0 // indirect
)
