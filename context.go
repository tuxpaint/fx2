package fx2

import (
	"context"

	"go.uber.org/fx"
)

// adds a context.Context which will be cancelled OnStop()
func Context(lc fx.Lifecycle) context.Context {
	b, cn := context.WithCancel(context.Background())
	lc.Append(fx.Hook{
		OnStop: func(ctx context.Context) error {
			cn()
			return nil
		},
	})
	return b
}
