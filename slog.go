package fx2

import (
	"log/slog"

	"go.uber.org/fx"
	"go.uber.org/fx/fxevent"
)

var WithSlog = fx.WithLogger(func(log *slog.Logger) fxevent.Logger {
	r := &fxevent.SlogLogger{Logger: log}
	r.UseLogLevel(slog.LevelDebug)
	return r
})
